library(rentrez)

x = read.table( "sampling.csv", header=TRUE, sep=",", stringsAsFactors=FALSE )

get_species_number <- function( sp_name ){
	term = paste( '(', sp_name, '[Subtree]) AND "specified"[Properties]', sep='' )
	n = entrez_search(db="taxonomy", term=term)$count
	return(n)
}

x[,3] = sapply( x[,1], get_species_number )

write.table( x, "sampling_updated.csv", sep=",", row.names=FALSE )