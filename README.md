# Published Animal Genome Sequences


![Figure 1](Figure1.png)


A phylogenetic summary of the number of published animal genomes, in relation to the numbers of described species and species with molecular data. These numbers are shown on a log scale.


This figure was originally generated for the manuscript:

> Dunn, CW and JF Ryan. 2015. The Evolution of Animal Genomes. Current Opinion in Genetics and Development 35:25-32. [doi:10.1016/j.gde.2015.08.006](http://dx.doi.org/10.1016/j.gde.2015.08.006)

Are we missing a published genome? Do you have better estimates on numbers of species for any of these clades? Please let us know via the [issue tracker](https://bitbucket.org/caseywdunn/animal-genomes/issues?status=new&status=open).


## Contents

This repository contains:

- `Figure1.pdf` An animal phylogeny with the number of described species, number of species with molecular sequence data, and number of published genomes mapped to the tips.
- `Figure1.png` A preview manually rendered from `Figure1.pdf`.
- `draw_tree.r` The source code for rendering the figure.
- `Tree_tree.tre` An animal phylogeny topology from [Dunn et al. 2014](http://dx.doi.org/10.1146/annurev-ecolsys-120213-091627)
- `update_ncbi_records.r` Script for retrieving the umber of animal species with molecular sequence data.
- `sampling.csv` The data that are mapped onto the tree tips.

## Execution

To render the figure from the data, run the following at the shell:

    Rscript draw_tree.r

## Data sources

The data in `sampling.csv` come from several sources.

The estimated numbers of species are drawn mostly from [Appeltans et al. 2012](http://dx.doi.org/10.1016/j.cub.2012.09.036). Where ranges were given, the average was used. The arthropod number is from [Hamilton et al. 2010](http://dx.doi.org/10.1086/652998).

The numbers of described species are drawn from [Giga Community of Scientists 2014](http://dx.doi.org/10.1093/jhered/est084).

The number of species with molecular data is compiled from NCBI with the script `update_ncbi_records.r`.

The numbers of published genomes are from http://wikipedia.org/wiki/List_of_sequenced_animal_genomes, which we updated.